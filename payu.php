<?php

$GATEWAYMODULE["payuname"]="payu";
$GATEWAYMODULE["payuvisiblename"]="Payu";
$GATEWAYMODULE["payutype"]="Invoices";

function payu_activate() {
  defineGatewayField("payu","text","merchantId","","Id de comerciante","6","Ej.: <strong>500111</strong>");
  defineGatewayField("payu","text","ApiKey","","Clave","Ej.: <strong>1u77nqhq7ftd0hlvnjfs77ah1c</strong>");
  defineGatewayField("payu","text","currency","" ,"Moneda","Ej.: <strong>501000</strong>");
  defineGatewayField("payu","text","accountId","","ID Cuenta País","Ej.: <strong>501000</strong>");
  defineGatewayField("payu","text","buyerEmail","","Correo de vendedor","Ej.: <strong>info@hostindg.com</strong>");
  defineGatewayField("payu","text","adminUser","","Usuario admin de whmcs","Ej.: Admin de WHmcs");
}

function payu_link($params) {
   # Gateway Specific Variables
   $gatewaymerchantId = $params['merchantId'];
   $gatewayApiKey = $params['ApiKey'];
   $gatewayreferenceCode = "Pago cuenta # " .$params['$invoiceid'];
   $gatewaycurrency = $params['currency'];
   $gatewayaccountId = $params['accountId'];
   $gatewaybuyerEmail = $params['buyerEmail'];
   # Invoice Variables
   $invoiceid = $params['invoiceid'];
   $description = $params["description"];
   $amount = $params['amount'];
   $currency = $params['currency'];
   $duedate = $params['duedate'];
   # Client Variables
   $firstname = $params['clientdetails']['firstname'];
   $lastname = $params['clientdetails']['lastname'];
   $email = $params['clientdetails']['email'];
   $address1 = $params['clientdetails']['address1'];
   $address2 = $params['clientdetails']['address2'];
   $city = $params['clientdetails']['city'];
   $state = $params['clientdetails']['state'];
   $postcode = $params['clientdetails']['postcode'];
   $country = $params['clientdetails']['country'];
   $phone = $params['clientdetails']['phonenumber'];
   # System Variables
   $companyname = $params['companyname'];
   $systemurl = $params['systemurl'];
   $currency = $params['currency'];
   $adminUser = $params['adminUser'];
   # End of Variables

   //print_r($params);

   // Redondeo de centavos. 0.4 lo redondea hacia abajo, 0.5 lo redondea hacia arriba.
	$amount = round($amount);
  $signature = $gatewayApiKey.'~'.$gatewaymerchantId.'~'.$invoiceid.'~'.$amount.'~'.$gatewaycurrency;
  $signature = md5($signature);

  $code .= '<form action="https://gateway.payulatam.com/ppp-web-gateway" method="post" target="_blank">
  <input type="hidden" value="'.$gatewaymerchantId.'" name="merchantId">
  <input type="hidden" value="'.$gatewayapikey.'" name="ApiKey">
  <input type="hidden" value="'.$invoiceid.'" name="referenceCode">
  <input type="hidden" value="'.$gatewayaccountId.'" name="accountId">
  <input type="hidden" value="'.$description.'" name="description">
  <input type="hidden" value="'.$amount.'" name="amount">
  <input type="hidden" value="0" name="tax">
  <input type="hidden" value="0" name="taxReturnBase">
  <input type="hidden" value="'.$gatewaycurrency.'" name="currency">
  <input type="hidden" value="'.$signature.'" name="signature">
  <input type="hidden" value="'.$email.'" name="buyerEmail">
  <input type="hidden" value="'.$firstname." ".$lastname.'" name="payerFullName">
  <input name="confirmationUrl" type="hidden"  value="http://' . $_SERVER['HTTP_HOST'] .'/modules/gateways/payu/confirmation.php?u='.$adminUser.'" >
  <input type="submit" class="skenda" value="Pagar"/>
  </form>';

return $code;

}


?>
